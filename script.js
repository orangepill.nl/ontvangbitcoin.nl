function includeHTML(callback) {
  const elements = document.querySelectorAll("[data-include-html]");
  let elementsToLoad = elements.length; // Count how many elements need to be loaded

  elements.forEach(async function (element) {
    const file = element.getAttribute("data-include-html");

    if (file) {
      try {
        const response = await fetch(file);

        if (!response.ok) {
          throw new Error(`Failed to load: ${file}`);
        }

        const html = await response.text();
        element.innerHTML = html;
        element.removeAttribute("data-include-html");
        elementsToLoad--;

        initializeEventListeners();

        // If all elements are loaded, call the callback
        if (callback && elementsToLoad === 0) callback();
      } catch (error) {
        console.error(error);
        element.innerHTML = "Page not found.";
      }
    }
  });
}

function initializeEventListeners() {
  const menuButton = document.querySelector(".menu-button");
  if (menuButton) {
    menuButton.addEventListener("click", showSideBar);
  } else {
    console.error("Menu button not found!");
  }

  const closeButton = document.querySelector(".close");
  if (closeButton) {
    closeButton.addEventListener("click", hideSidebar);
  } else {
    console.error("Close button not found!");
  }

  // Selecting the navigation bar
  const nav = document.querySelector("nav");
  if (nav) {
    window.addEventListener("scroll", function () {
      if (window.scrollY > 50) {
        nav.classList.add("shadow");
      } else {
        nav.classList.remove("shadow");
      }
    });
  }
}

function showSideBar() {
  const sidebar = document.querySelector(".sidebar");
  sidebar.style.display = "flex";
  sidebar.style.transform = "translateX(0)";
}

function hideSidebar() {
  const sidebar = document.querySelector(".sidebar");
  sidebar.style.display = "none"; // This hides the sidebar
}

document.addEventListener("DOMContentLoaded", function () {
  includeHTML(initializeEventListeners);
});
