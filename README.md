# ontvangbitcoin.nl
Because this is a Dutch website the content below is in Dutch. Feel free to use code / template for your own project.

## Omschrijving
Vrije open community website https://ontvangbitcoin.nl waar Nederlandse ondernemers die bitcoin willen ontvangen laagdrempelig kennis kunnen opdoen. Teksten zijn daarbij in de taal/cultuur van ondernemers omschreven. Zoveel mogelijk technische termen worden ontweken. De site bestaat uit secties zoals demo, praktijk cases, vraag en antwoord, hoe te starten.

## Installatie
De bestanden zijn statische HTML, CSS, Javascript. Deze kunnen oa met een [git pull](https://docs.gitlab.com/ee/user/project/repository/mirror/pull.html) naar de lokale omgeving geplaatst worden om daar te zien of te bewerken.

## Ondersteuning
Heb je vragen over de code dan kun je via [Signal Messenger contact opnemen](https://signal.group/#CjQKIFx6SClVlZWU8yjnw8UpEMUuBctLu6D0N7Z_I9OIUTE6EhDs529FNlBOL058S48Pq9zc) met de community ontwikkel groep van ontvangbitcoin.nl

## Auteurs
ontvangbitcoin.nl is met ❤️ gemaakt door enthousiaste Bitcoin community bijdragers.

Samalka Hansana,
Jurjen de Vries,
Anke Buurma,
Mario Zijlstra,
Arjan Yspeert,
Doeks,
Edwin Comino,
Jacco,
Richy

## Bijdragen
Heb je toevoegingen of verbeteringen in de code of inhoud, voel je vrij een fork te maken en een PR in te dienen. Wil je actiever bijdragen dan kun je ook via [Signal Messenger](https://signal.group/#CjQKIFx6SClVlZWU8yjnw8UpEMUuBctLu6D0N7Z_I9OIUTE6EhDs529FNlBOL058S48Pq9zc) aansluiten bij de community van ontvangbitcoin.nl om direct toegang tot deze repository te verkrijgen.

## Licentie
De code is uitgegeven middels [MIT licentie](https://ontvangbitcoin.nl/LICENSE). De inhoud is beschikbaar als [CC0 / Public Domain](https://creativecommons.org/publicdomain/zero/1.0/deed.nl).